# README

## Installation
* `bundle install`

## Running tests
* `rspec spec spec/*`

## Build Docker
* `docker build -t symantec_rails:latest .`
* `docker image ls` to see your new docker image
* `docker run -p 3000:3000 symantec_rails:latest`

## View Live
* visit url `http://localhost:3000`

## Push Docker to the cloud (for Kubernetes)
* `docker push symantec_rails:latest`