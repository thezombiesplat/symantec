class Exoplanet
    @exoplanets = []
    def initialize(json=[])
        if json.empty?
            json = JSON.load(Net::HTTP.get(URI.parse("https://gist.githubusercontent.com/joelbirchler/66cf8045fcbb6515557347c05d789b4a/raw/9a196385b44d4288431eef74896c0512bad3defe/exoplanets")))
        end
        @exoplanets = json
    end

    def getHottest
        hottest = {
            "HostStarTempK" => "0"
        }
        @exoplanets.each do |exoplanet|
            if exoplanet['HostStarTempK'].to_i > hottest['HostStarTempK'].to_i
                hottest = exoplanet
            end
        end
        return hottest
    end

    def getOrphanCount
        orphanCount = 0
        @exoplanets.each do |exoplanet|
            if exoplanet['TypeFlag'] == 3
                orphanCount += 1
            end
        end
        return orphanCount
    end

    def getTimeline
        timeline = {}
        @exoplanets.each do |exoplanet|
            if !exoplanet['RadiusJpt'].to_s.strip.empty? && !exoplanet['DiscoveryYear'].to_s.strip.empty?
                if !timeline[exoplanet['DiscoveryYear'].to_s]
                    timeline[exoplanet['DiscoveryYear'].to_s] = {
                        "small" => {},
                        "medium" => {},
                        "large" => {}
                    }
                end
                if exoplanet['RadiusJpt'].to_f > 0 && exoplanet['RadiusJpt'].to_f < 1
                    timeline[exoplanet['DiscoveryYear'].to_s]['small'][exoplanet['PlanetIdentifier']] = exoplanet
                elsif exoplanet['RadiusJpt'].to_f >= 1 && exoplanet['RadiusJpt'].to_f < 2
                    timeline[exoplanet['DiscoveryYear'].to_s]['medium'][exoplanet['PlanetIdentifier']] = exoplanet
                elsif exoplanet['RadiusJpt'].to_f > 2
                    timeline[exoplanet['DiscoveryYear'].to_s]['large'][exoplanet['PlanetIdentifier']] = exoplanet
                end
            end
        end
        timeline = timeline.sort
        return timeline
    end
end
