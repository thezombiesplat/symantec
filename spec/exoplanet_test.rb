require '../app/models/exoplanet'

threeOrphans = [
    {
        "TypeFlag" => 0,
    },
    {
        "TypeFlag" => 1,
    },
    {
        "TypeFlag" => 2,
    },
    {
        "TypeFlag" => 3,
    },
    {
        "TypeFlag" => 1,
    },
    {
        "TypeFlag" => 2,
    },
    {
        "TypeFlag" => 3,
    },
    {
        "TypeFlag" => 1,
    },
    {
        "TypeFlag" => 2,
    },
    {
        "TypeFlag" => 3,
    },
    {
        "TypeFlag" => 1,
    },
    {
        "TypeFlag" => 2,
    },
    {
        "TypeFlag" => 0,
    },
    {
        "TypeFlag" => 0,
    }
]
zeroOrphans = [
    {
        "TypeFlag" => 1,
    },
    {
        "TypeFlag" => 2,
    },
    {
        "TypeFlag" => 0,
    },
    {
        "TypeFlag" => 1,
    },
    {
        "TypeFlag" => 2,
    },
    {
        "TypeFlag" => 0,
    },
    {
        "TypeFlag" => 0,
    },
]

hottestStarSet = [
    {
        "HostStarTempK" => 2000
    },
    {
        "HostStarTempK" => -100
    },
    {
        "HostStarTempK" => 200
    },
    {
        "HostStarTempK" => 123
    },
    {
        "HostStarTempK" => 5000,
        "PlanetIdentifier" => "Hottest"
    },
    {
        "HostStarTempK" => 20
    },
    {
        "HostStarTempK" => 10
    },
]
timelineSet = [
  {
    "PlanetIdentifier" => "HD 13808 c",
    "RadiusJpt" => 0,
    "DiscoveryYear" => 2011,
  },
  {
    "PlanetIdentifier" => "HD 13808 c not made up",
    "RadiusJpt" => 1.5,
    "DiscoveryYear" => 2011,
  },
  {
    "PlanetIdentifier" => "KOI-1894.01",
    "RadiusJpt" => 0.66,
    "DiscoveryYear" => 2015,
  },
  {
    "PlanetIdentifier" => "Kepler-450 b",
    "RadiusJpt" => 0.548,
    "DiscoveryYear" => 2015,
  },
  {
    "PlanetIdentifier" => "Kepler-450 c",
    "RadiusJpt" => 0.234,
    "DiscoveryYear" => 2015,
  },
  {
    "PlanetIdentifier" => "Kepler-450 d",
    "RadiusJpt" => 0.0747,
    "DiscoveryYear" => 2015,
  },
  {
    "PlanetIdentifier" => "Kepler-1316 b",
    "RadiusJpt" => 0.316,
    "DiscoveryYear" => 2016,
  },
  {
    "PlanetIdentifier" => "HD 175607 b",
    "RadiusJpt" => "",
    "DiscoveryYear" => 2015,
  },
  {
    "PlanetIdentifier" => "HD 175607 c",
    "RadiusJpt" => "",
    "DiscoveryYear" => 2015,
  },
  {
    "PlanetIdentifier" => "Kepler-1032 b",
    "RadiusJpt" => 0.167,
    "DiscoveryYear" => 2016,
  },
  {
    "PlanetIdentifier" => "Kepler-1616 b",
    "RadiusJpt" => 0.091,
    "DiscoveryYear" => 2016,
  },
  {
    "PlanetIdentifier" => "HIP 63242 b",
    "RadiusJpt" => "",
    "DiscoveryYear" => 2013,
  },
  {
    "PlanetIdentifier" => "Kepler-929 b",
    "RadiusJpt" => 9.099,
    "DiscoveryYear" => 2016,
  },
]

describe Exoplanet do
    @@threeOrphans = threeOrphans
    @@zeroOrphans = zeroOrphans
    @@hottestStarSet = hottestStarSet
    @@timelineSet = timelineSet

    it "Calculates 3 orphan planets" do
        exoplanets = Exoplanet.new(@@threeOrphans)
        expect(exoplanets.getOrphanCount).to be 3
    end

    it "Calculates 0 orphan planets" do
        exoplanets = Exoplanet.new(@@zeroOrphans)
        expect(exoplanets.getOrphanCount).to be 0
    end

    it "Finds the planet orbiting the hottest star" do
        exoplanets = Exoplanet.new(@@hottestStarSet)
        expect(exoplanets.getHottest['PlanetIdentifier']).to eq "Hottest"
    end
    
    it "creates a timeline of exoplanets discovered per year" do
        exoplanets = Exoplanet.new(@@timelineSet)
        timeline = exoplanets.getTimeline
        expect(timeline.size).to eq 3

        #Check the labels of the rows
        expect(timeline[0][0]).to eq "2011"
        expect(timeline[1][0]).to eq "2015"
        expect(timeline[2][0]).to eq "2016"

        # Checks for a medium planet
        expect(timeline[0][1]['small'].size).to eq 0
        expect(timeline[0][1]['medium'].size).to eq 1
        expect(timeline[0][1]['large'].size).to eq 0

        # Checks for a small planet
        expect(timeline[1][1]['small'].size).to eq 4
        expect(timeline[1][1]['medium'].size).to eq 0
        expect(timeline[1][1]['large'].size).to eq 0

        # Checks for a large planet
        expect(timeline[2][1]['small'].size).to eq 3
        expect(timeline[2][1]['medium'].size).to eq 0
        expect(timeline[2][1]['large'].size).to eq 1
    end
end
